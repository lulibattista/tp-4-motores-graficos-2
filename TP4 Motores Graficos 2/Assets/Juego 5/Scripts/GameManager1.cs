using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class GameManager1 : MonoBehaviour
{
    public GameObject previousTile;
    public GameObject currentTile;
    public GameObject tilePrefab;
    public int score = 1;
    public TMP_Text ScoreText;
    public GameObject GameOverMenu;


    public float speed = 10f;

    private bool gamePaused = false;
    private bool gameOver = false;

    void Start()
    {
        tilePrefab.GetComponent<Rigidbody>();
    }



    void Update()
    {
        if (!gamePaused && !gameOver) // Verificar si el juego no est� pausado ni ha terminado
        {
            if (Input.GetMouseButtonDown(0))
            {
                DealWithPlayerClick();
            }
            if (currentTile != null)
            {
                currentTile.transform.localScale += new Vector3(1, 0, 1) * speed;
            }
        }
    }

    public void DealWithPlayerClick()
    {
        if (currentTile == null && !gameOver) // Evitar instanciar nuevos tiles cuando el juego ha terminado
        {
            currentTile = Instantiate(tilePrefab, previousTile.transform.position + Vector3.up, previousTile.transform.rotation);
        }
        else
        {
            if (previousTile.transform.localScale.x > currentTile.transform.localScale.x)
            {
                previousTile = currentTile;
                currentTile = Instantiate(tilePrefab, previousTile.transform.position + Vector3.up, previousTile.transform.rotation);
                Camera.main.transform.position += Vector3.up;
                score++;
                ScoreText.text = score.ToString();
            }
            else if (!gameOver) // Evitar activar el men� de Game Over m�s de una vez
            {
                GameOverMenu.SetActive(true);
                gameOver = true;
                PauseGame();
            }
        }
    }

    public void Retry()
    {
        SceneManager.LoadScene("Juego5");
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void PauseGame()
    {
        Time.timeScale = 0f; // Pausar el tiempo en el juego
        gamePaused = true; // Marcar el juego como pausado
    }
}
