using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa5 : MonoBehaviour
{
    public GameObject pausemenu5;
    public string sceneName5;
    public bool toggle5;
    public Controller_Player playerScript5;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggle5 = !toggle5;
            if (toggle5 == false)
            {
                pausemenu5.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                playerScript5.enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (toggle5 == true)
            {
                pausemenu5.SetActive(true);
                AudioListener.pause = true;
                Time.timeScale = 0;
                playerScript5.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void resumeGame()
    {
        toggle5 = false;
        pausemenu5.SetActive(false);
        AudioListener.pause = false;
        Time.timeScale = 1;
        playerScript5.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void quitToMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(sceneName5);
    }
    public void quitToDesktop()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        Application.Quit();
    }
}
