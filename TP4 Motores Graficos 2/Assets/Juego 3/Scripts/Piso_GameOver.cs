using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Piso_GameOver : MonoBehaviour
{
    public bool IsCollision;
    // Start is called before the first frame update
    void Start()
    {
        IsCollision = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            IsCollision = true;
            Debug.Log("Game Over");
            //Application.Quit();

        }
    }

    public void Retry()
    {
        SceneManager.LoadScene("Juego1");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
