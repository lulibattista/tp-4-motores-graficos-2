using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Platform : MonoBehaviour
{
    public float velocidadRotacion = 100f;
    private bool rotating = false;
    private bool rotating2 = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            rotating = true;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            rotating = false;
        }

        if (rotating)
        {
            transform.Rotate(Vector3.forward, velocidadRotacion * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            rotating2 = true;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            rotating2 = false;
        }
        if (rotating2)
        {
            transform.Rotate(-Vector3.forward, velocidadRotacion * Time.deltaTime);
        }
    }
}
