using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Instantiator_Controller : MonoBehaviour
{
    public List<GameObject> Proyectiles;
    public GameObject instantiatePos;
    public GameObject GameOver;

    // Start is called before the first frame update
    void Start()
    {
        Control_Bala.GameOverScreen = false;
        GameOver.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnProyectiles();
        }
        if (Control_Bala.GameOverScreen == true)
        {
            GameOver.SetActive(true);
        }
    }
    private void SpawnProyectiles()
    {
        
        Instantiate(Proyectiles[0], instantiatePos.transform.position, instantiatePos.transform.rotation);


        
    }
    public void Retry()
    {
        SceneManager.LoadScene("Juego2");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
