using UnityEngine;

public class Rotacion : MonoBehaviour
{
    public float velocidadRotacion = 20f; // velocidad a la que la pelota rotar�

    void Update()
    {
        
        
        // rota la pelota en su propio eje en la direcci�n hacia adelante
        transform.Rotate(Vector3.forward, velocidadRotacion * Time.deltaTime);
        
    }
}
